var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
    res.sendFile(__dirname+'/index.html');
  });

io.on('connection',function(socket){
    console.log('** new user connected')
    //io.emit('chat message', 'hi');
     socket.broadcast.emit('hi'); //except sender
    
    socket.on('chat message', function(msg){
        //console.log('message: ' + msg);
        io.emit('chat message', msg);
      });

    socket.on('disconnect', function(){
        console.log('* user disconnected');
      });
})
const port = process.env.PORT || 8080;

http.listen(port,function(){
    console.log('listening on * :'+port)
})